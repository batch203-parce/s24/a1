//console.log("Hello World!");

/*
Instructions for s24 Activity:
1. In the S24 folder, create an a1 folder and an index.html and script.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
14. Create a git repository named S24.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.
*/

// Output 1
const getCube = Math.pow(2, 3);
console.log(`The cube of 2 is ${getCube}`);


// Output 2
const address = ["258 Washington Ave NW", "California 90011"];
console.log(`I live at ${address[0]}, ${address[1]}`);


// Output 3
const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	measurement: "20 ft 3 in",
}
const {name, type, weight, measurement} = animal;
console.log(`${animal.name} was a ${animal.type}. He weighed at ${animal.weight} kgs with a measurement of ${animal.measurement}.`);

/*function getAnimal({name, type, weight, measurement}){
	console.log(`${animal.name} was a ${animal.type}. He weighed at ${animal.weight} kgs with a measurement of ${animal.measurement}.`);
}
getAnimal(animal);*/

// Output 4
const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(`${number}`))

// Output 5
const reduceNumber = numbers.reduce((previousValue, currentValue) => previousValue + currentValue);
console.log(reduceNumber);


// Output 6
class Dog{
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}
	const myDog = new Dog();
	myDog.name = "Frankie";
	myDog.age = 5;
	myDog.breed = "Miniature Dachshund";
	console.log(myDog);
